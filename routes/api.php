<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('docentes','DocentesController');
Route::apiResource('estudiantes','EstudiantesController');
Route::apiResource('eventos','EventoController');
Route::apiResource('asignaturas','AsignaturasController');
Route::apiResource('horarios_aulas','HorariosAulasController')->only(['index','store']);
Route::apiResource('aulas','AulasController');

Route::get('clases','ClaseController@index');
Route::post('clases/programar','ClaseController@crearClaseBloque');
Route::post('clases/sugerir-horas-asignatura/{claseId?}','ClaseController@sugerirHorasAsignatura');
Route::post('clases/sugerir-salones/{claseId?}','ClaseController@sugerirSalones');
Route::post('clases/programar-individual/{claseId?}','ClaseController@crearClaseIndividual');
Route::delete('clases/cancelar/{id}','ClaseController@cancelarClase');
Route::post('clases/update-description/{id}','ClaseController@updateDescription');

Route::get('users','UserController@index');
Route::put('user/restore-password/{id}','UserController@restorePassword');
Route::post('user/change-password/{id}','UserController@changePassword');
Route::get('user/asignaturas/{id}','UserController@misAsignaturas');
Route::get('user/clases/{id}','UserController@misClases');


Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');

Route::post('general/load-data', 'GeneralController@loadData');
Route::get('general/parameters', 'GeneralController@getParameters');
Route::post('general/parameters-update', 'GeneralController@updateParameters');
