<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

use Tymon\JWTAuth\JWTAuth;


class JwtMiddleware
{
    /**
     * The JWT Authenticator.
     *
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $auth;

    /**
     * Create a new BaseMiddleware instance.
     *
     * @param \Tymon\JWTAuth\JWTAuth $auth
     *
     * @return void
     */
    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!$this->auth->parser()->setRequest($request)->hasToken()) {
                return response()->json([
                    'error' => 'No hay token de autenticación'
                ], 401);
            }
            if (!$this->auth->parseToken()->authenticate()) {
                return response()->json([
                    'error' => 'Usuario no encontrado'
                ],
                    401
                );
            }
        } catch (TokenExpiredException $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ], 401);
        } catch (JWTException $e) {
            return response()->json([
                'error' => 'Token no válido'
            ], 401);
        }

        return $next($request);
    }
}

