<?php

namespace App\Http\Controllers;

use App\Asignatura;
use App\Aula;
use App\Clase;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\HorariosAula;
use App\Imports\TemporalAsignaturaImport;
use App\Mail\ClaseCanceladaMail;
use App\Mail\ClaseReprogramadaMail;
use App\Parameter;
use App\TemporalAsignatura;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ClaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN);
    }

    private $matrizClases = [];
    private $posibilidades = [];
    private $posibilidadesSalones = [];
    private $posibilidadesSalonesDocentes = [];
    private $matrizSalones = [];
    private $weekMap = [
        0 => 'Domingo',
        1 => 'Lunes',
        2 => 'Martes',
        3 => 'Miércoles',
        4 => 'Jueves',
        5 => 'Viernes',
        6 => 'Sábado',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('no_pagination')){
                //SI NO HAY PAGINACIÓN
                $clases = Clase::query()->get();
            }else {
                //SI HAY PAGINACIÓN
                //CAPTURAMOS TODOS LOS INPUTS Y LOS FILTROS
                $termino = $request->has('termino') ? $request->input('termino') : "";
                $searchAsignatura = $request->has('search_asignatura') ? $request->input('search_asignatura') : "";
                $searchFecha = $request->has('search_fecha') ? $request->input('search_fecha') : "";
                $searchAula = $request->has('search_aula') ? $request->input('search_aula') : "";
                $searchDocente = $request->has('search_docente') ? $request->input('search_docente') : "";
                $limit = $request->input('per_page');
                $orderBy = $request->input('order_by');
                $order = $request->input('order');

                $query = Clase::query()->where('fecha','like','%'.$searchFecha.'%')
                    ->whereHas('asignatura',function ($q) use ($searchAsignatura){
                        $q->where('nombre','like','%'.$searchAsignatura.'%')
                            ->orWhere('codigo','like','%'.$searchAsignatura.'%');
                    });

                //FILTRO AULA
                if($searchAula && $searchAula !== "Sin salón") {
                    $query->whereHas('aula',function ($q) use ($searchAula){
                        $q->where('nombre','like','%'.$searchAula.'%');
                    });
                }else if($searchAula === "Sin salón"){
                    $query->whereDoesntHave('aula');
                }

                //FILTRO DOCENTE
                if($searchDocente) {
                    $query->whereHas('asignatura.docente',function ($q) use ($searchDocente){
                        $q->where('name','like','%'.$searchDocente.'%');
                    });
                }

                //FILTRO ORBER_BY
                if($orderBy !== 'aula' && $orderBy !== 'asignatura.codigo' && $orderBy !== 'asignatura.nombre' && $orderBy !== 'asignatura.docente.name') {
                    //SI LA COLUMNA DE EL ORDER_BY ES NORMALITO (PERTENECE A LA TABLA DE CLASES)
                    $query->with(['aula', 'asignatura.docente'])->orderBy($orderBy, $order);
                }else{
                    //SI LA COLUMNA DEL ORDER_BY PERTENECE A ALGUNA RELACIÓN
                    $query->with(['aula' => function ($q) use ($order, $orderBy){
                        $q->orderBy('nombre',$order);
                    }, 'asignatura' => function ($q) use ($order, $orderBy){
                        if($orderBy === 'asignatura.nombre'){
                            $q->orderBy('nombre',$order);
                        }elseif ($orderBy === 'asignatura.codigo'){
                            $q->orderBy('codigo',$order);
                        }
                    },'asignatura.docente' => function ($q) use ($order, $orderBy){
                        $q->orderBy('name',$order);
                    }]);
                }
                //FILTRO PAGINACIÓN
                if(intval($limit) > 0) {
                    //SI SE QUIERE PAGINAR NORMAL
                    $clases = $query->paginate($limit);
                }else{
                    //SI SE QUIEREN MOSTRAR TODOS LOS REGISTROS EN UNA SOLA PÁGINA (PER_PAGE === -1)
                    $clases = $query->get();
                    $clases = ['data' => $clases];
                }
            }
            return Response::responseSuccess('', CodesResponse::CODE_OK, $clases);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    public function crearClaseBloque(Request $request)
    {
        try{
            /*
             * REQUEST:
             * fecha: 2020-09-14
             * asignaturas: [1,2,4,5]
             * horarios:[
             * {
             *  hora: 7:00:00
             *  hora_fin: 9:00:00
             * },
             * {
             *  hora: 9:00:00
             *  hora_fin: 11:00:00
             * }]
             */

            //BUSCAR CADA ASIGNATURA PERO DAR PRIORIDAD A LAS QUE TENGAN MÁS ESTUDIANTES
            //Dependiendo de si mandaron un archivo con los códigos o no
            if($request->asignaturas_file){
                Excel::import(new TemporalAsignaturaImport(), $request->file('asignaturas_file'));
                $asignaturasCodigo = TemporalAsignatura::query()->pluck('codigo');
                $asignaturas = Asignatura::query()->with('estudiantes','docente')->whereIn('codigo',$asignaturasCodigo)
                    ->get()->sortBy(function($asignatura) {
                        return $asignatura->estudiantes->count();
                    },3,true);
                TemporalAsignatura::query()->delete();
            }else{
                $asignaturas = Asignatura::query()->with('estudiantes','docente')->whereIn('id',$request->asignaturas)
                    ->get()->sortBy(function($asignatura) {
                        return $asignatura->estudiantes->count();
                    },3,true);
            }

            DB::beginTransaction();

            //EXCEPCIONES
            $asignaturasNoHayDocente = [];
            $asignaturasDocenteNoDisponible = [];
            $asignaturasDescartadasPorCruces = []; //las que se le cruzan a algún estudiantes

            $horarios = json_decode($request->horarios, true);
            $this->llenarMatrizSalones($request->fecha, $horarios);

            //RECORREMOS LAS ASIGNATURAS
            foreach ($asignaturas as $asignatura){
                if($asignatura->docente_id) {
                    //BUSCAMOS EL PRIMER HORARIO EN QUE EL DOCENTE ESTÉ LIBRE
                    $asignaturasDocente = Asignatura::query()->where('docente_id', $asignatura->docente_id)
                        ->pluck('id');
                    $clasesDelDocenteParaEseDia = Clase::query()->whereIn('asignatura_id', $asignaturasDocente)
                        ->where('fecha', $request->fecha)->get();
                    $horariosLibresDocente = $this->getHorariosLibres($horarios, $clasesDelDocenteParaEseDia);
                    if (count($horariosLibresDocente) === 0) {
                        array_push($asignaturasDocenteNoDisponible, $asignatura);
                    } else {
                        //VERIFICAR QUÉ HORARIOS DE LOS $horariosLibresDocente ESTÁN LIBRES PARA TODOS LOS ESTUDIANTES
                        $horariosLibresFinales = $horariosLibresDocente;
                        foreach ($asignatura->estudiantes as $estudiante) {
                            $asignaturasEstudiante = $estudiante->asignaturasEstudiante->pluck('id')->all();
                            $clasesDelEstudianteParaEseDia = Clase::query()
                                ->whereIn('asignatura_id', $asignaturasEstudiante)
                                ->where('fecha', $request->fecha)
                                ->get();
                            $horariosLibresFinales = $this->getHorariosLibres($horariosLibresFinales, $clasesDelEstudianteParaEseDia);
                        }
                        if (count($horariosLibresFinales) > 0) {
                            array_push($this->posibilidades, [
                                "horasPosibles" => $horariosLibresFinales,
                                "estudiantes" => $asignatura->estudiantes->pluck('id')->all(),
                                "docente" => $asignatura->docente_id,
                                "asignatura" => $asignatura->id,
                            ]);
                        } else {
                            array_push($asignaturasDescartadasPorCruces, $asignatura);
                        }
                    }
                }else{
                    array_push($asignaturasNoHayDocente, $asignatura);
                }
            }

            /*
             * De la parte anterior vamos a obtener un arreglo llamado posibilidades, y sus items seran como los siguientes:
             *  [
             *  "horasPosibles"=>[
             *      ["hora"=>7:00, "hora_fin"=>9:00],
             *      ["hora"=>9:00, "hora_fin"=>11:00],
             *  ],
             *  "estudiantes"=>[1,2,3,4,5],
             *  "docente"=>2,
             *  "asignatura"=>3,
             * ]
             *  De esta manera vamos a tener un listado de posibles horas para cada asignatura con los IDs de los estudiantes que
             * la componen y el ID del docente. Ahora recorremos las posibilidades y llenamos la matriz con 1 y 0
             */
            if(count($this->posibilidades) > 0){
                $resp = $this->recorrerPosibilidades();
                $clasesEscogidas = $resp["vectorClasesEscogidas"];
                $asignaturasDescartadas = $resp["asignaturasDescartadas"];
            }else{
                $clasesEscogidas = [];
                $asignaturasDescartadas = [];
            }

            /*
             *  El anterior método nos va a retornar las clases escogidas en este formato:
                *  0 => [
                      "horasPosibles" => array:2 [
                        "hora" => "7:00:00"
                        "hora_fin" => "9:00:00"
                      ]
                      "asignatura" => 17
                    ]
                    1 =>  [
                      "horasPosibles" => array:2 [
                        "hora" => "11:00:00"
                        "hora_fin" => "1:00:00"
                      ]
                      "asignatura" => 17
                    ]
             */
            //debemos agregar las asignaturas que no quedaron seleccionadas al arreglo de $asignaturasDescartadasPorCruces
            foreach ($asignaturas as $asignatura){
                if(in_array($asignatura->id, $asignaturasDescartadas)){
                    array_push($asignaturasDescartadasPorCruces, $asignatura);
                }
            }

            $clasesProgramadas = 0;
            //ahora si comenzamos a guardar las clases en la base de datos
            foreach ($clasesEscogidas as $claseEscogida){
                $claseDB = new Clase();
                $claseDB->hora = $claseEscogida["horasPosibles"]["hora"];
                $claseDB->hora_fin = $claseEscogida["horasPosibles"]["hora_fin"];
                $claseDB->fecha = $request->fecha;
                $claseDB->asignatura_id = $claseEscogida["asignatura"];
                $claseDB->aula_id = $this->getAula($claseEscogida["docente"], $claseEscogida["horasPosibles"]);
                $claseDB->save();
                $clasesProgramadas += 1;
            }
            $data = [
                'asignaturasNoHayDocente'  => $asignaturasNoHayDocente,
                'asignaturasDocenteNoDisponible'  => $asignaturasDocenteNoDisponible,
                'asignaturasDescartadasPorCruces'  => $asignaturasDescartadasPorCruces,
                'clasesProgramadas'  => $clasesProgramadas,
            ];
            DB::commit();
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$data);
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $errors = [];

            foreach ($failures as $failure) {
                array_push($errors,[
                    'fila'=> $failure->row(), // row that went wrong
                    'columna'=>$failure->attribute(),
                    'error'=>$failure->errors()[0],
                ]);
            }

            return Response::responseError('Hay errores en el archivo', CodesResponse::CODE_BAD_REQUEST, $errors);
        }catch (ModelNotFoundException $e){
            DB::rollBack();
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            DB::rollBack();
            return Response::responseError($e->getMessage().' '.$e->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Este método llena el arreglo de posibilidadesSalones con la siguiente estructura:
     *  idSalon => array:2 [
    0 => array:2 [
    "hora" => "9:00:00"
    "hora_fin" => "11:00:00"
    ]
    1 => array:2 [
    "hora" => "11:00:00"
    "hora_fin" => "1:00:00"
    ]
    ]
     * Es decir, se llena con los intervalos posibles para cada salón teniendo en cuenta las clases programadas en
     * ese salón y la hora de apertura y cierre. Además llena el arreglo de matrizSalones que tiene
     * los mismos índices y se llena con 1(s).
     * @param $fecha
     * @param $horarios
     */
    private function llenarMatrizSalones($fecha, $horarios){
        $diaSemana = $this->weekMap[Carbon::createFromFormat('Y-m-d',$fecha)->dayOfWeek];
        $horariosAulas = HorariosAula::query()->where('dia_semana',$diaSemana)->get();

        /*
         * Agregamos todos los keys de las aulas disponibles a
         * posibilidadesSalonesDocentes, para que más adelante vayamos especificando que docentes
         * tienen clases en que salón
         */
        foreach ($horariosAulas as $aula){
            if(!array_key_exists($aula->aula_id, $this->posibilidadesSalonesDocentes)) {
                $this->posibilidadesSalonesDocentes[$aula->aula_id] = [];
            }
        }
        foreach ($horariosAulas as $aula){
            //trae los intervalos disponibles para el salon teniendo en cuenta
            //la hora de inicio y la hora de cierre del salón
            $horariosDisponibles = $this->getHorariosDisponiblesSalon($horarios, $aula);
            $clasesProgramadas = Clase::query()->with('asignatura')->where('aula_id',$aula->aula_id)
                ->whereDate('fecha',$fecha)->get();
            //trae los horarios libres teniendo en cuenta las clases programadas para ese día en este salón
            $horariosDisponibles = $this->getHorariosLibres($horariosDisponibles, $clasesProgramadas);
            /*
             * Vamos a llenar la matriz de posibilidadesSalonesDocentes con los ids de los docentes que
             * a tienen clases programadas en este Salón
             */
            foreach ($clasesProgramadas as $clase){
                if (!in_array($clase->asignatura->docente_id, $this->posibilidadesSalonesDocentes[$aula->aula_id])) {
                    array_push($this->posibilidadesSalonesDocentes[$aula->aula_id], $clase->asignatura->docente_id);
                }
            }
            /*
             * llenamos el vector de posisbilidadesSalones y la matriz de Salones
             */
            $this->posibilidadesSalones[$aula->aula_id] = $horariosDisponibles;
            $vectorHorarios = [];
            $i = 0;
            while ($i<count($horariosDisponibles)){
                array_push($vectorHorarios, $aula->aula->cantidad_docentes);
                $i++;
            }
            $this->matrizSalones[$aula->aula_id] = $vectorHorarios;
        }
    }

    /**
     * Dado un $docenteId y un $intervaloHoras se busca en la matriz de salones
     * retorna el primer salón posible
     * @param $docenteId
     * @param $intervaloHoras
     * @return int|string|null
     */
    private function getAula($docenteId, $intervaloHoras){
        //buscamos en la matriz de posibilidadesSalonesDocentes si este docente ya tiene alguna clase programada
        //en alguno de los salones posibles
        foreach ($this->posibilidadesSalonesDocentes as $aulaId=>$docentes){
            if(in_array($docenteId, $docentes)){
                //si el docente tiene clases programadas en este salon, buscamos si ese salón
                //tiene el intervalo que buscamos libre
                foreach ($this->posibilidadesSalones[$aulaId] as $j=>$intervaloLibre){
                    if($intervaloHoras === $intervaloLibre && $this->matrizSalones[$aulaId][$j] > 0){
                        //escogemos este intervalo y retornamos el aula_id
                        //marcamos que ese intervalo del aula ya no va a estar disponible
                        $this->matrizSalones[$aulaId][$j] -= 1;
                        return $aulaId;
                    }
                }
            }
        }
        //si llega hasta aqui es porque el docente no tenia ningún salón. En ese caso, buscamos el primer
        //salón que esté vacío
        foreach ($this->posibilidadesSalonesDocentes as $aulaId=>$docentes){
            if(count($docentes) === 0){
                //si el salón no tiene docentes, buscamos si ese salón
                //tiene el intervalo que buscamos libre
                foreach ($this->posibilidadesSalones[$aulaId] as $j=>$intervaloLibre){
                    if($intervaloHoras === $intervaloLibre && $this->matrizSalones[$aulaId][$j] > 0){
                        //escogemos este intervalo y retornamos el aula_id
                        //marcamos que ese intervalo del aula ya no va a estar disponible
                        $this->matrizSalones[$aulaId][$j] -= 1;
                        array_push($this->posibilidadesSalonesDocentes[$aulaId], $docenteId);
                        return $aulaId;
                    }
                }
            }
        }
        //si no tiene salon y no hay ningún salón vacío, escogemos el primer
        //salón de los que estén disponibles que tenga el intervalo libre
        foreach ($this->posibilidadesSalones as $aulaId=>$intervalos){
            foreach ($intervalos as $j=>$intervaloLibre){
                if($intervaloHoras === $intervaloLibre && $this->matrizSalones[$aulaId][$j] > 0){
                    //escogemos este intervalo y retornamos el aula_id
                    //marcamos que ese intervalo del aula ya no va a estar disponible
                    $this->matrizSalones[$aulaId][$j] -= 1;
                    //agregamos el docente_id a posibilidadesSalonesDocentes para que las siguientes clases
                    //también se programen en este salón
                    array_push($this->posibilidadesSalonesDocentes[$aulaId], $docenteId);
                    //$this->posibilidadesSalonesDocentes[$aulaId][$j] = $docenteId;
                    return $aulaId;
                }
            }
        }

        //si no encontró nada retornamos null;
        return null;
    }

    /**
     * Habiendo escogido una posición en la matriz, se descartan todas las que deban descartarse por cruces,
     * es decir, se marcan en 0
     * @param $i
     * @param $j
     */
    private function descartarPosibilidadesMatriz($i, $j){
        $posibilidades = $this->posibilidades;
        $descartarEstudiantes = $posibilidades[$i]["estudiantes"];
        $descartarDocente = $posibilidades[$i]["docente"];
        $descartarAsignatura = $posibilidades[$i]["asignatura"];
        $intervaloHoras = $posibilidades[$i]["horasPosibles"][$j];

        foreach ($posibilidades as $i2=>$posibilidad){
            if($i2 >= $i) {
                //si tiene el mismo docente o al menos un estudiante relacionado
                if ($posibilidad["docente"] === $descartarDocente ||
                    count(array_diff($descartarEstudiantes, $posibilidad["estudiantes"])) !== count($descartarEstudiantes)) {
                    //si es la misma asignatura no la va a agregar
                    if ($posibilidad["asignatura"] !== $descartarAsignatura) {
                        //descartamos solo el intervalo que se cruce
                        foreach ($posibilidad["horasPosibles"] as $j2 => $intervalo) {
                            if ($intervalo === $intervaloHoras) {
                                $this->matrizClases[$i2][$j2] = 0;
                            }
                        }
                    } else {
                        //si es la misma asignatura descartamos todas las demas franjas horarias
                        foreach ($posibilidad["horasPosibles"] as $j2 => $intervalo) {
                            if ($intervalo !== $intervaloHoras) {
                                $this->matrizClases[$i2][$j2] = 0;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * organiza las posibilidades y guarda en la matrizClases una matriz de 1 y 0 dependiendo de las clases que
     * se puedan tomar o no. La matriz de 1 y 0 es equivalente a la matriz de posibilidades, las filas son
     * asignaturas, las columnas son las franjas horarias. Cada columna debe terner al final un solo 1 con la franja escogida
     * y lo demás en 0
     */
    private function recorrerPosibilidades(){
        /*
         * organizar el arreglo de posibilidades de tal manera que primero estén los que tengan menos posibilidades
         */
        $posibilidadesHelper = [];
        $posibilidades = $this->posibilidades;
        $numMinimoPosibilidades = count($posibilidades[0]["horasPosibles"]);
        $numMaximoPosibilidades = count($posibilidades[0]["horasPosibles"]);
        //buscamos cual es el número minimo y máximo de horas posibles en todas las asignaturas
        foreach ($posibilidades as $posibilidad){
            if(count($posibilidad["horasPosibles"]) < $numMinimoPosibilidades){
                $numMinimoPosibilidades = count($posibilidad["horasPosibles"]);
            }elseif (count($posibilidad["horasPosibles"]) > $numMaximoPosibilidades){
                $numMaximoPosibilidades = count($posibilidad["horasPosibles"]);
            }
        }
        //vamos formando un nuevo arreglo, agregando primero las que tienen el minimo de horasPosibles,
        //a los que tienen más horasPosibles
        for($i = $numMinimoPosibilidades; $i<=$numMaximoPosibilidades; $i++){
            foreach ($posibilidades as $posibilidad){
                if(count($posibilidad["horasPosibles"]) === $i){
                    array_push($posibilidadesHelper, $posibilidad);
                    //alimentamos la matriz con 1 en cada posición
                    $vectorHorarios = [];
                    $i = 0;
                    while ($i<count($posibilidad["horasPosibles"])){
                        array_push($vectorHorarios, 1);
                        $i++;
                    }
                    array_push($this->matrizClases, $vectorHorarios);
                }
            }
        }
        //actualizamos el arreglo de posibilidades
        $this->posibilidades = $posibilidadesHelper;
        $vectorClasesEscogidas = [];
        $asignaturasDescartadas = [];
        //ahora vamos a escoger las primeras franjas horarias de todas las posibilidades
        foreach ($this->matrizClases as $i=>$fila){
            $descartada = true;
            foreach ($fila as $j=>$columna){
                if($this->matrizClases[$i][$j] === 1){
                    $asignatura = [];
                    $asignatura["horasPosibles"] = $posibilidades[$i]["horasPosibles"][$j];
                    $asignatura["asignatura"] = $posibilidades[$i]["asignatura"];
                    $asignatura["docente"] = $posibilidades[$i]["docente"];
                    array_push($vectorClasesEscogidas, $asignatura);
                    $descartada = false;
                    break;
                }
            }
            if($descartada){
                array_push($asignaturasDescartadas, $posibilidades[$i]["asignatura"]);
            }else{
                $this->descartarPosibilidadesMatriz($i, $j);
            }

        }
        return ["vectorClasesEscogidas"=>$vectorClasesEscogidas, "asignaturasDescartadas"=>$asignaturasDescartadas];
    }

    /**
     * Verifica que horarios sugeridos se cruza con que clases y retorna los
     * horarios libres
     * @param $horariosSugeridos
     * @param $clasesProgramadas
     * @return array
     */
    private function getHorariosLibres($horariosSugeridos, $clasesProgramadas){
        $horariosLibres = [];
        foreach ($horariosSugeridos as $horario){
            $seCruzaConClase = false;
            foreach ($clasesProgramadas as $clase){
                if($this->getEnteroHora($clase->hora) <= $this->getEnteroHora($horario["hora"])){
                    if($this->getEnteroHora($clase->hora_fin) > $this->getEnteroHora($horario["hora"])){
                        $seCruzaConClase = true;
                    }
                }else if($this->getEnteroHora($clase->hora_fin) >= $this->getEnteroHora($horario["hora_fin"])){
                    if($this->getEnteroHora($clase->hora) < $this->getEnteroHora($horario["hora_fin"])){
                        $seCruzaConClase = true;
                    }
                }
            }
            if(!$seCruzaConClase){
                array_push($horariosLibres, $horario);
            }
        }
        return $horariosLibres;
    }

    /**
     * Busca dentro de una sugerencia de horarios (intervalos) cuales estarian disponibles
     * para una asignatura en especifico, cuidando que estén libres tanto para el docente
     * como para los estudiantes
     * @param $asignatura
     * @param $fecha
     * @param $horarios
     * @return array
     */
    private function getHorariosLibresAsignatura($asignatura, $fecha, $horarios){
        //BUSCAMOS EL PRIMER HORARIO EN QUE EL DOCENTE ESTÉ LIBRE
        $asignaturasDocente = Asignatura::query()->where('docente_id', $asignatura->docente_id)
            ->pluck('id');
        $clasesDelDocenteParaEseDia = Clase::query()->whereIn('asignatura_id', $asignaturasDocente)
            ->where('fecha', $fecha)->get();
        $horariosLibresDocente = $this->getHorariosLibres($horarios, $clasesDelDocenteParaEseDia);
        $horariosLibresFinales = $horariosLibresDocente;
        //VERIFICAR QUÉ HORARIOS DE LOS $horariosLibresDocente ESTÁN LIBRES PARA TODOS LOS ESTUDIANTES
        foreach ($asignatura->estudiantes as $estudiante) {
            $asignaturasEstudiante = $estudiante->asignaturasEstudiante->pluck('id')->all();
            $clasesDelEstudianteParaEseDia = Clase::query()
                ->whereIn('asignatura_id', $asignaturasEstudiante)
                ->where('fecha', $fecha)
                ->get();
            $horariosLibresFinales = $this->getHorariosLibres($horariosLibresFinales, $clasesDelEstudianteParaEseDia);
        }
        return $horariosLibresFinales;
    }


    private function getHorariosDisponiblesSalon($horariosSugeridos, $intervaloSalon){
        $horariosDisponibles = [];
        foreach ($horariosSugeridos as $horario){
            if($this->getEnteroHora($horario["hora"]) >= $this->getEnteroHora($intervaloSalon->hora_inicio)
                && $this->getEnteroHora($horario["hora_fin"]) <= $this->getEnteroHora($intervaloSalon->hora_fin)){
                array_push($horariosDisponibles, $horario);
            }
        }
        return $horariosDisponibles;
    }

    private function getEnteroHora($hora){
        $horaStr = str_replace(":","",$hora);
        while (strlen($horaStr) < 6){
            $horaStr = $horaStr."0";
        }
        return intval($horaStr);
    }


    /**
     * Dado una fecha y una asignatura, se busca todos los horarios disponibles desde la 00:00 a la
     * 23:00 que no se le crucen a ningún estudiante ni al docente
     * @param Request $request
     * @param null $claseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function sugerirHorasAsignatura(Request $request, $claseId = null)
    {
        try{
            $validation = Validator::make($request->all(),[
                'fecha'=> 'required',
                'asignatura'=> 'required',
            ]);
            if($validation->fails()){
                return Response::responseError(['error'=> $validation->errors()->all()[0]], CodesResponse::CODE_FORM_INVALIDATE);
            }else{
                $asignatura = Asignatura::query()->findOrFail($request->asignatura);
                //construimos un arreglo de horas posibles que tenga todos los intervalos desde las 00:00 hasta las 24:00
                $duracionClase = Parameter::query()->first();
                $duracionClase = $duracionClase->duracion_clase;
                $horasPosibles = [];
                for($i=0; $i<24; $i++){
                    $horaFin = $i+$duracionClase;
                    if($horaFin === 24){
                        $horaFin = 0;
                    }
                    if($horaFin === 25){
                        $horaFin = 1;
                    }
                    $hora = [
                        'hora'=>strlen($i) === 1 ? '0'.$i.':00' : $i.':00',
                        'hora_fin'=>strlen($horaFin) === 1 ? '0'.$horaFin.':00' : $horaFin.':00',
                    ];
                    array_push($horasPosibles, $hora);
                }
                //buscamos cual de esas horas sugeridas no se le cruza a ninguno
                $horasPosibles = $this->getHorariosLibresAsignatura($asignatura, $request->fecha, $horasPosibles);
                //si recibimos la claseId es porque solo queremos reprogramar, en ese caso incluimos tambien la hora
                //de la clase actual
                if($claseId){
                    $clase = Clase::query()->findOrFail($claseId);
                    $hora = explode(":",$clase->hora)[0].':00';
                    $horaFin = explode(":",$clase->hora_fin)[0].':00';
                    array_push($horasPosibles, ["hora"=>$hora, "hora_fin"=>$horaFin]);
                    usort($horasPosibles, function($a, $b) {
                        return $a['hora'] <=> $b['hora'];
                    });
                }
                return Response::responseSuccess('', CodesResponse::CODE_OK, $horasPosibles);
            }
        }catch (\Exception $e){
            return Response::responseError($e->getMessage().' '.$e->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }


    /**
     * Dado una fecha y una franja horaria especifica, se buscan los salones que esten
     * disponibles a esa hora y en los que no exista ninguna clase
     * @param Request $request
     * @param null $claseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function sugerirSalones(Request $request, $claseId = null)
    {
        try{
            $validation = Validator::make($request->all(),[
                'fecha'=> 'required',
                'hora'=> 'required',
            ]);
            if($validation->fails()){
                return Response::responseError(['error'=> $validation->errors()->all()[0]], CodesResponse::CODE_FORM_INVALIDATE);
            }else{
                $aulasDisponibles = [];
                $diaSemana = $this->weekMap[Carbon::createFromFormat('Y-m-d',$request->fecha)->dayOfWeek];
                $horariosAulas = HorariosAula::query()->where('dia_semana',$diaSemana)->get();
                $horario = $request->hora;
                foreach ($horariosAulas as $horarioAula){
                    //chequeamossi el aula está abierta
                    if($this->getEnteroHora($horario["hora"]) >= $this->getEnteroHora($horarioAula->hora_inicio)
                        && $this->getEnteroHora($horario["hora_fin"]) <= $this->getEnteroHora($horarioAula->hora_fin)){
                        //chequeamos que no haya otra clase a esa hora ignorando la claseId que nos enviaron
                        $otrasClasesCount = Clase::query()->where('fecha',$request->fecha)->whereKeyNot($claseId)
                            ->where('aula_id',$horarioAula->aula_id)
                            ->whereBetween('hora',[$horario["hora"], $horario["hora_fin"]])
                            ->count();
                        //verificamos cuantas clases son permitidas en esa aula
                        $aula = Aula::query()->findOrFail($horarioAula->aula_id);
                        if($otrasClasesCount < $aula->cantidad_docentes){
                            array_push($aulasDisponibles, $horarioAula->aula_id);
                        }
                    }
                }
                $aulas = Aula::query()->whereIn('id',$aulasDisponibles)->get();
                return Response::responseSuccess('', CodesResponse::CODE_OK, $aulas);
            }
        }catch (\Exception $e){
            return Response::responseError($e->getMessage().' '.$e->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Simplemente se crea la clase individual con los datos necesarios
     * asumiendo que ya se hicieron todas las validaciones necesarias
     * @param Request $request
     * @param null $claseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function crearClaseIndividual(Request $request, $claseId = null)
    {
        DB::beginTransaction();
        try{
            $validation = Validator::make($request->all(),[
                'fecha'=> 'required',
                'hora'=> 'required',
                'asignatura'=> 'required',
                'aula'=> 'required',
            ]);

            if($validation->fails()){
                return Response::responseError(['error'=> $validation->errors()->all()[0]], CodesResponse::CODE_FORM_INVALIDATE);
            }else{
                $mailData = new \stdClass();
                //si recibimos la claseId es porque solo queremos reprogramar, en ese caso buscamos la clase
                //para actualizarla
                if($claseId){
                    $claseDB = Clase::with('asignatura.estudiantes','asignatura.docente','aula')->findOrFail($claseId);
                    $claseAntigua = clone $claseDB;
                }else{
                    $claseDB = new Clase();
                }
                $claseDB->hora = $request->hora["hora"];
                $claseDB->hora_fin = $request->hora["hora_fin"];
                $claseDB->fecha = $request->fecha;
                $claseDB->asignatura_id = $request->asignatura;
                $claseDB->aula_id = $request->aula;
                $claseDB->save();

                $claseDB = Clase::with('asignatura.estudiantes','asignatura.docente','aula')->findOrFail($claseDB->id);
                //enviamos notificación al correo si es una reprogramación
                if($claseId){
                    $usersEmail = [];
                    foreach ($claseDB->asignatura->estudiantes as $estudiante){
                        array_push($usersEmail, $estudiante->email);
                    }
                    array_push($usersEmail, $claseDB->asignatura->docente->email);
                    $mailData->claseNueva = $claseDB;
                    $mailData->clase = $claseAntigua;
                    Mail::to($usersEmail)->send(new ClaseReprogramadaMail($mailData));
                }
                DB::commit();
                return Response::responseSuccess('', CodesResponse::CODE_OK, $claseDB);
            }
        }catch (\Exception $e){
            DB::rollBack();
            return Response::responseError($e->getMessage().' '.$e->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Borra una clase
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelarClase($id)
    {
        DB::beginTransaction();
        try{
            $clase = Clase::with('asignatura.estudiantes','asignatura.docente','aula')->findOrFail($id);
            $usersEmail = [];
            foreach ($clase->asignatura->estudiantes as $estudiante){
                array_push($usersEmail, $estudiante->email);
            }
            array_push($usersEmail, $clase->asignatura->docente->email);
            //Mail::to($usersEmail)->send(new ClaseCanceladaMail($clase));
            $clase->delete();
            DB::commit();
            return Response::responseSuccess('', CodesResponse::CODE_OK, $clase);
        }catch (\Exception $e){
            DB::rollBack();
            return Response::responseError($e->getMessage().' '.$e->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * actualizar la descripción de la clase
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDescription($id, Request $request)
    {
        try{
            $validation = Validator::make($request->all(),[
                'description'=> 'required',
            ]);
            if($validation->fails()){
                return Response::responseError(['error'=> $validation->errors()->all()[0]], CodesResponse::CODE_FORM_INVALIDATE);
            }else {
                $clase = Clase::query()->findOrFail($id);
                $clase->description = $request->description;
                $clase->save();
                return Response::responseSuccess('', CodesResponse::CODE_OK, $clase);
            }
        }catch (\Exception $e){
            return Response::responseError($e->getMessage().' '.$e->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }
}
