<?php

namespace App\Http\Controllers;

use App\Aula;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\HorariosAula;
use App\Http\Requests\AulaRequest;
use App\Http\Requests\HorariosAulasRequest;
use App\User;
use Illuminate\Http\Request;

class HorariosAulasController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('no_pagination')){
                $horariosAulas = HorariosAula::query()->get();
            }else {
                $termino = "";
                if ($request->has('termino')) {
                    $termino = $request->input('termino');
                }
                $limit = $request->input('per_page');
                $orderBy = $request->input('order_by');
                $order = $request->input('order');
                //si per_page = -1 no paginamos
                if(intval($limit) > 0) {
                    $horariosAulas = HorariosAula::query()->buscarCoincidencia($termino)
                        ->with('aula')
                        ->orderBy($orderBy, $order)
                        ->paginate($limit);
                }else{
                    $horariosAulas = HorariosAula::query()->buscarCoincidencia($termino)
                        ->with('aula')
                        ->orderBy($orderBy, $order)
                        ->get();
                    $horariosAulas = ['data'=>$horariosAulas];
                }
            }
            return Response::responseSuccess('', CodesResponse::CODE_OK, $horariosAulas);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param AulaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(HorariosAulasRequest $request)
    {
        /*
         * El formulario que consume este método debe:
         * 1) Tener un checkbox con los días de la semana,
         * es decir que voy a recibir un arreglo llamado dias_semana
         * 2) Tener un input de tags para agregar una o varias aulas,
         * puede que ya estén creadas o que no, es decir que voy a recibir un arreglo llamado
         * aulas con los nombres de las aulas
         * 3) Un date picker para la hora_inicio
         * 4) un date picker para la hora_fin
         * 5) Un input para agregar el número máximo de profesores que comparten esa aula
         *      (que solo se activa si se marca un checkbox que dice "este salón
         *      puede tener clases compartidas")
         */
        try{
            if($request->no_disponible){
                //SI LO MARCARON COMO NO DISPONIBLE ENTONCES BORRAMOS TODOS LOS HORARIOS_AULAS
                foreach ($request->aulas as $aula){
                    $aula = strtoupper($aula);
                    $aulaDB = Aula::query()->where('nombre',$aula)->first();
                    if($aulaDB){
                        $horariosAulaDB = HorariosAula::query()
                            ->where('aula_id',$aulaDB->id)
                            ->get();
                        foreach ($horariosAulaDB as $horario){
                            $horario->delete();
                        }
                    }
                }
            }else{
                foreach ($request->dias_semana as $diaSemana){
                    foreach ($request->aulas as $aula){
                        $aula = strtoupper($aula);
                        $aulaDB = Aula::query()->where('nombre',$aula)->first();
                        if(!$aulaDB){
                            $aulaDB = new Aula();
                            $aulaDB->nombre = $aula;
                        }
                        $aulaDB->cantidad_docentes = ($request->has('cantidad_docentes') ? $request->get('cantidad_docentes') : 1);
                        $aulaDB->save();
                        $horarioAulaDB = HorariosAula::query()
                            ->where('aula_id',$aulaDB->id)
                            ->where('dia_semana',$diaSemana)->first();
                        if(!$horarioAulaDB){
                            $horarioAulaDB = new HorariosAula();
                            $horarioAulaDB->aula_id = $aulaDB->id;
                            $horarioAulaDB->dia_semana = $diaSemana;
                        }
                        $horarioAulaDB->hora_inicio = $request->hora_inicio;
                        $horarioAulaDB->hora_fin = $request->hora_fin;
                        $horarioAulaDB->save();
                    }
                }
            }


            return Response::responseSuccess(true,CodesResponse::CODE_OK, $aula);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

}
