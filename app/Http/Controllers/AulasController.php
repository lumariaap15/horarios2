<?php

namespace App\Http\Controllers;

use App\Aula;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\AulaRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AulasController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $aulas = Aula::query()
                ->orderBy('nombre')
                ->get();
            return Response::responseSuccess('', CodesResponse::CODE_OK, $aulas);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param AulaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AulaRequest $request)
    {
        try{
            $aula = new Aula();
            $aula->fill($request->all());
            $aula->save();
            return Response::responseSuccess(true,CodesResponse::CODE_OK, $aula);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Muestra información del estudiante, sus asignaturas y clases
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $aula = Aula::with('horarios')->findOrFail($id);
            return Response::responseSuccess('',CodesResponse::CODE_OK,$aula);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AulaRequest $request, $id)
    {
        try{
            $aula = Aula::query()->findOrFail($id);
            $aula->fill($request->all());
            $aula->save();
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$aula);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $aula = Aula::query()->with('clases')->findOrFail($id);
            if(count($aula->clases) > 0){
                return Response::responseError('No puedes borrar esta aula, ya que se relaciona con algunas clases',
                    CodesResponse::CODE_BAD_REQUEST);
            }else{
                $aula->delete();
                return Response::responseSuccess(true,CodesResponse::CODE_OK,$aula);
            }
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }
}
