<?php

namespace App\Http\Controllers;

use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\EstudianteRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EstudiantesController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('no_pagination')){
                $estudiantes = User::query()
                    ->where('role', User::ROLE_ESTUDIANTE)
                    ->orderBy('name')
                    ->get();
            }else {
                $termino = "";
                if ($request->has('termino')) {
                    $termino = $request->input('termino');
                }
                $limit = $request->input('per_page');
                $orderBy = $request->input('order_by');
                $order = $request->input('order');
                //si per_page = -1 no paginamos
                if(intval($limit) > 0) {
                    $estudiantes = User::query()->buscarCoincidencia($termino)
                        ->where('role', User::ROLE_ESTUDIANTE)
                        ->orderBy($orderBy, $order)
                        ->paginate($limit);
                    foreach ($estudiantes->items() as $estudiante) {
                        $estudiante->asignaturas = $estudiante->asignaturasEstudiante()->pluck('asignaturas.id')->toArray();
                    }
                }else{
                    $estudiantes = User::query()->buscarCoincidencia($termino)
                        ->where('role', User::ROLE_ESTUDIANTE)
                        ->orderBy($orderBy, $order)
                        ->get();
                    foreach ($estudiantes as $estudiante) {
                        $estudiante->asignaturas = $estudiante->asignaturasEstudiante()->pluck('asignaturas.id')->toArray();
                    }
                    $estudiantes = ['data'=>$estudiantes];
                }
            }
            return Response::responseSuccess('', CodesResponse::CODE_OK, $estudiantes);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstudianteRequest $request)
    {
        try{
            $estudiante = new User();
            $estudiante->fill($request->all());
            $estudiante->role = User::ROLE_ESTUDIANTE;
            $estudiante->password = bcrypt($request->identificacion);
            $estudiante->save();
            $estudiante->asignaturasEstudiante()->attach($request->asignaturas);
            return Response::responseSuccess('El estudiante se creó correctamente.',CodesResponse::CODE_OK, $estudiante);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Muestra información del estudiante, sus asignaturas y clases
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $estudiante = User::with('asignaturasEstudiante.clases.aula','asignaturasEstudiante.docente')->findOrFail($id);
            foreach ($estudiante->asignaturasEstudiante as $asignatura){
                $asignatura->clases = $this->formatoClases($asignatura->clases);
            }
            return Response::responseSuccess('',CodesResponse::CODE_OK,$estudiante);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EstudianteRequest $request, $id)
    {
        try{
            $estudiante = User::query()->findOrFail($id);
            $estudiante->fill($request->all());
            $estudiante->save();
            $estudiante->asignaturasEstudiante()->sync($request->asignaturas);
            return Response::responseSuccess('El estudiante se actualizó correctamente',CodesResponse::CODE_OK,$estudiante);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = User::query()->findOrFail($id);
            $user->delete();
            return Response::responseSuccess('El estudiante se eliminó correctamente',CodesResponse::CODE_OK,$user);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }
}
