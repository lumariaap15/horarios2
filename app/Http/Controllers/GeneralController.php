<?php

namespace App\Http\Controllers;

use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Imports\AsignaturaUserImport;
use App\Parameter;
use App\Rules\ExcelRule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class GeneralController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify')->except(['getParameters']);;
        $this->middleware('role:'.User::ROLE_ADMIN)->except(['getParameters']);
    }

    /**
     * Carga el excel de los datos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadData(Request $request)
    {
        
        try{
            set_time_limit(-1);
            $validation = Validator::make($request->all(),[
                'file'=> new ExcelRule($request->file('file'))
            ]);
            if($validation->fails()){
                return Response::responseError(['error'=> $validation->errors()->all()[0]], CodesResponse::CODE_FORM_INVALIDATE);
            }else{
                Excel::import(new AsignaturaUserImport(), $request->file('file'));
                return Response::responseSuccess(true,CodesResponse::CODE_OK,null);
            }
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $errors = [];

            foreach ($failures as $failure) {
                array_push($errors,[
                   'fila'=> $failure->row(), // row that went wrong
                    'columna'=>$failure->attribute(),
                    'error'=>$failure->errors()[0],
                ]);
            }

            return Response::responseError('Hay errores en el archivo', CodesResponse::CODE_BAD_REQUEST, $errors);
        }
        catch (\Exception $exception){
            return Response::responseError($exception->getMessage().' '. $exception->getLine(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Trae el parametro general definido
     * @return \Illuminate\Http\JsonResponse
     */
    public function getParameters()
    {
        try{
            $config = Parameter::query()->first();
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$config);
        }catch (\Exception $exception){
            return Response::responseError($exception->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * actualiza la configuración general
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateParameters(Request $request)
    {
        try{
            $config = Parameter::query()->first();
            $duracion = $request->has('duracion_clase') ?  $request->get('duracion_clase') : $config->duracion_clase;
            if($request->has('logo_institucion')){
                $logo = Storage::disk('public')->put('public',$request->logo_institucion);
                $config->logo_institucion = $logo;
            }else if($request->has('borrar_logo')){
                $config->logo_institucion = null;
                Storage::delete($config->logo_institucion);
            }
            $config->duracion_clase = $duracion;
            $config->save();
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$config);
        }catch (\Exception $exception){
            return Response::responseError($exception->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }
}
