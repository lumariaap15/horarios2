<?php

namespace App\Http\Controllers;

use App\Asignatura;
use App\Evento;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\EventoRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EventoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $termino = "";
            if ($request->has('termino')) {
                $termino = $request->input('termino');
            }
            $limit = $request->input('per_page');
            $orderBy = $request->input('order_by');
            $order = $request->input('order');
            //si per_page = -1 no paginamos
            if(intval($limit) > 0) {
                $eventos = Evento::with('users')->buscarCoincidencia($termino)
                    ->orderBy($orderBy, $order)
                    ->paginate($limit);
            }else{
                $eventos = Evento::with('users')->buscarCoincidencia($termino)
                    ->orderBy($orderBy, $order)
                    ->get();
                $eventos = ['data'=>$eventos];
            }
            return Response::responseSuccess('', CodesResponse::CODE_OK, $eventos);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventoRequest $request)
    {
        try{
            $evento = new Evento();
            $evento->fill($request->all());
            $evento->save();
            $evento->users()->attach($request->users);
            if($request->asignaturas){
                $a = Asignatura::with('estudiantes')->whereIn('codigo',$request->asignaturas)->get();
                foreach ($a as $asignatura){
                    $evento->users()->syncWithoutDetaching($asignatura->estudiantes);
                }
            }
            return Response::responseSuccess('El evento se creó correctamente.',CodesResponse::CODE_OK, $evento);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Muestra información del evento
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $evento = Evento::with('users')->findOrFail($id);
            return Response::responseSuccess('',CodesResponse::CODE_OK,$evento);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventoRequest $request, $id)
    {
        try{
            $evento = Evento::query()->findOrFail($id);
            $evento->fill($request->all());
            $evento->save();
            $evento->users()->sync($request->users);
            if($request->asignaturas){
                $a = Asignatura::with('estudiantes')->whereIn('codigo',$request->asignaturas)->get();
                foreach ($a as $asignatura){
                    $evento->estudiantes()->syncWithoutDetaching($asignatura->estudiantes);
                }
            }
            return Response::responseSuccess('El evento se actualizó correctamente',CodesResponse::CODE_OK,$evento);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $item = Evento::query()->findOrFail($id);
            $item->delete();
            return Response::responseSuccess('El evento se eliminó correctamente',CodesResponse::CODE_OK,$item);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }
}
