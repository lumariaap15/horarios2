<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['login']]);
    }

    /**
     * @OA\Post(
     *     tags={"Autenticacion"},
     *     path="/api/login",
     *     summary="Login de usuario",
     *
     *   @OA\RequestBody(
     *       @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="email", type="string", example="admin@gmail.com"),
     *         @OA\Property(property="password", type="string", example="password")
     *       )
     *   ),
     *     @OA\Response(
     *         response=200,
     *         description="Usuario autenticado."
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Credenciales incorrectas."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Estas credenciales no coinciden con nuestros registros'], 401);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }
    public function payload()
    {
        return response()->json(auth('api')->payload());
    }

    /**
     * @OA\Post(
     *     tags={"Autenticacion"},
     *     path="/api/logout",
     *     summary="cerrar sesion",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Usuario deslogeado."
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Credenciales incorrectas."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function logout()
    {
        auth('api')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * @OA\Post(
     *     tags={"Autenticacion"},
     *     path="/api/refresh",
     *     summary="Renovar token de autenticacion",
     *     security={{"bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Nuevo token de autenticacion."
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Credenciales incorrectas."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        //$roles = auth('api')->user()->roles;
        $user = User::query()->findOrFail(auth('api')->user()->id);
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => $user
        ]);
    }
}
