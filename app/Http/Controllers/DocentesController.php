<?php

namespace App\Http\Controllers;

use App\Asignatura;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\DocenteRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class DocentesController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('no_pagination')){
               $docentes = User::query()
                   ->where('role', User::ROLE_DOCENTE)
                   ->orderBy('name')
                   ->get();
            }else {
                $termino = "";
                if ($request->has('termino')) {
                    $termino = $request->input('termino');
                }
                $limit = $request->input('per_page');
                $orderBy = $request->input('order_by');
                $order = $request->input('order');
                //si per_page = -1 no paginamos
                if(intval($limit) > 0) {
                    $docentes = User::query()->buscarCoincidencia($termino)
                        ->where('role', User::ROLE_DOCENTE)
                        ->orderBy($orderBy, $order)
                        ->paginate($limit);
                    foreach ($docentes->items() as $docente) {
                        $docente->asignaturas = $docente->asignaturasDocente()->pluck('asignaturas.id')->toArray();
                    }
                }else{
                    $docentes = User::query()->buscarCoincidencia($termino)
                        ->where('role', User::ROLE_DOCENTE)
                        ->orderBy($orderBy, $order)
                        ->get();
                    foreach ($docentes as $docente) {
                        $docente->asignaturas = $docente->asignaturasDocente()->pluck('asignaturas.id')->toArray();
                    }
                    $docentes = ['data'=>$docentes];
                }
            }
            return Response::responseSuccess('', CodesResponse::CODE_OK, $docentes);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocenteRequest $request)
    {
        try{
            $docente = new User();
            $docente->fill($request->all());
            $docente->role = 'Docente';
            $docente->password = bcrypt($request->identificacion);
            $docente->save();
            $asignaturas = Asignatura::query()->whereIn('id',$request->asignaturas)->get();
            foreach ($asignaturas as $asignatura){
                $asignatura->docente_id = $docente->id;
                $asignatura->save();
            }
            return Response::responseSuccess('El docente se creó correctamente.',CodesResponse::CODE_OK, $docente);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Muestra información del docente, sus asignaturas y clases
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $docente = User::with('asignaturasDocente.clases.aula')->findOrFail($id);
            foreach ($docente->asignaturasDocente as $asignatura){
                $asignatura->clases = $this->formatoClases($asignatura->clases);
            }
            return Response::responseSuccess('',CodesResponse::CODE_OK,$docente);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocenteRequest $request, $id)
    {
        try{
            $docente = User::query()->findOrFail($id);
            $docente->fill($request->all());
            $docente->save();
            //hacemos un sync() de las asignaturas
            $borrarAsignaturas = Asignatura::query()->where('docente_id',$id)
                ->whereNotIn('id',$request->asignaturas)->get();
            foreach ($borrarAsignaturas as $a){
                $a->docente_id = null;
                $a->save();
            }
            $agregarAsignaturas = Asignatura::query()
                ->whereIn('id',$request->asignaturas)->get();
            foreach ($agregarAsignaturas as $asignatura){
                $asignatura->docente_id = $docente->id;
                $asignatura->save();
            }
            return Response::responseSuccess('El docente se actualizó correctamente',CodesResponse::CODE_OK,$docente);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $docente = User::query()->with('asignaturasDocente')->findOrFail($id);
            if(count($docente->asignaturasDocente) > 0){
                return Response::responseError('No puedes borrar el docente, ya que está relacionado con algunas asignaturas',
                    CodesResponse::CODE_BAD_REQUEST);
            }else{
                $docente->delete();
                return Response::responseSuccess('El docente se eliminó correctamente',CodesResponse::CODE_OK,$docente);
            }
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

}
