<?php

namespace App\Http\Controllers;

use App\Asignatura;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Http\Requests\AsignaturaRequest;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AsignaturasController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN,['except'=>['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('no_pagination')){
                $asignaturas = Asignatura::query()
                    ->orderBy('nombre')
                    ->get();
            }else {
                $termino = "";
                if ($request->has('termino')) {
                    $termino = $request->input('termino');
                }
                $limit = $request->input('per_page');
                $orderBy = $request->has('order_by') ? $request->input('order_by') : 'updated_at';
                $order = $request->has('order') ? $request->input('order') : 'desc';
                if (intval($limit) > 0) {
                    if ($orderBy !== 'docente') {
                        $asignaturas = Asignatura::query()->with('docente', 'estudiantes')
                            ->withCount('estudiantes', 'clases')
                            ->buscarCoincidencia($termino)
                            ->orderBy($orderBy, $order)
                            ->paginate($limit);
                    } else {
                        $asignaturas = Asignatura::with(['docente' => function ($q) use ($order) {
                            $q->orderBy('name', $order);
                        }, 'estudiantes'])
                            ->withCount('estudiantes', 'clases')
                            ->buscarCoincidencia($termino)
                            ->paginate($limit);
                    }
                } else {
                    if ($orderBy !== 'docente') {
                        $asignaturas = Asignatura::query()->with('docente', 'estudiantes')
                            ->withCount('estudiantes', 'clases')
                            ->buscarCoincidencia($termino)
                            ->orderBy($orderBy, $order)
                            ->get();
                    } else {
                        $asignaturas = Asignatura::with(['docente' => function ($q) use ($order) {
                            $q->orderBy('name', $order);
                        }, 'estudiantes'])
                            ->withCount('estudiantes', 'clases')
                            ->buscarCoincidencia($termino)
                            ->get();
                    }
                    $asignaturas = ['data' => $asignaturas];
                }
            }

            return Response::responseSuccess('', CodesResponse::CODE_OK, $asignaturas);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param AsignaturaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AsignaturaRequest $request)
    {
        try{
            $item = new Asignatura();
            $item->fill($request->all());
            $item->save();
            if($request->has('estudiantes')){
                $item->estudiantes()->sync($request->estudiantes);
            }
            return Response::responseSuccess(true,CodesResponse::CODE_OK, $item);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Muestra información de la asignatura, con sus clases, su docente y sus estudiantes
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $asignatura = Asignatura::with('clases.aula','docente','estudiantes')->findOrFail($id);
            $asignatura->clases = $this->formatoClases($asignatura->clases);
            return Response::responseSuccess('',CodesResponse::CODE_OK,$asignatura);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AsignaturaRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AsignaturaRequest $request, $id)
    {
        try{
            $item = Asignatura::query()->findOrFail($id);
            $item->fill($request->all());
            if($request->has('estudiantes')){
                $item->estudiantes()->sync($request->estudiantes);
            }
            $item->save();
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$item);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $item = Asignatura::query()->findOrFail($id);
            $item->delete();
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$item);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }
}
