<?php

namespace App\Http\Controllers;

use App\Asignatura;
use App\Globals\CodesResponse;
use App\Globals\Response;
use App\Mail\PasswordResetMail;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.verify');
        $this->middleware('role:'.User::ROLE_ADMIN,['except'=>['misClases','misAsignaturas','changePassword','index']]);
    }

    public function index()
    {
        try {
            $users = User::query()->whereIn('role',[User::ROLE_ESTUDIANTE, User::ROLE_DOCENTE])->get();
            return Response::responseSuccess('', CodesResponse::CODE_OK, $users);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Restaura la contraseña del usuario a su número de identificación
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restorePassword($id)
    {
        try{
            $user = User::query()->findOrFail($id);
            $user->password = bcrypt($user->identificacion);
            $user->save();
            Mail::to($user->email)->send(new PasswordResetMail($user));
            return Response::responseSuccess('La contraseña se restableció correctamente',CodesResponse::CODE_OK,$user);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * se le permite al usuario cambiar su contraseña
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function changePassword(Request $request, $id)
    {
        $respuesta = null;
        try{
            $validation = Validator::make($request->all(),[
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
                'current_password' => 'required'
            ]);

            if($validation->fails()){
                $respuesta = response()->json(['error'=> $validation->errors()->all()[0]],422);
            }else{
                $user = User::query()->findOrFail($id);
                if(Hash::check($request->current_password, $user->password)){
                    $user->password = bcrypt($request->password);
                    $user->save();
                    $respuesta = response()->json($user);
                }else{
                    $respuesta = response()->json(['error'=> 'La contraseña ingresada no es la actual contraseña'],400);
                }
            }
        }catch (ModelNotFoundException $exception){
            $respuesta = response()->json(['error'=> $exception->getMessage()],404);
        }catch (\Exception $exception){
            $respuesta = response()->json(['error'=> $exception->getMessage()],500);
        }finally{
            return $respuesta;
        }
    }

    /**
     * Retorna las asignaturas de un docente o un estudiante
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function misAsignaturas($id)
    {
        try{
            $user = User::query()->findOrFail($id);
            if($user->role === User::ROLE_DOCENTE){
                $asignaturas = Asignatura::query()->where('docente_id',$user->id)
                    ->withCount('estudiantes','clases')->get();
            }else{
                $user->load('asignaturasEstudiante');
                $asignaturas = Asignatura::query()->whereIn('id',$user->asignaturasEstudiante->pluck('id'))
                    ->withCount('estudiantes','clases')->get();
            }
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$asignaturas);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

    /**
     * Retorna las clases programadas para un docente o un estudiante
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function misClases($id)
    {
        try{
            $user = User::query()->findOrFail($id);
            $clases = [];
            if($user->role === User::ROLE_DOCENTE){
                $user->load('asignaturasDocente.clases.aula');
                foreach ($user->asignaturasDocente as $asignatura){
                    foreach ($this->formatoClases($asignatura->clases) as $clase){
                        $clase->asignatura = $asignatura->nombre;
                        $clase->docente_nombre = $user->name;
                        $clase->docente_email = $user->email;
                        array_push($clases,$clase);
                    }
                }
            }else{
                $user->load('asignaturasEstudiante.clases.aula');
                foreach ($user->asignaturasEstudiante as $asignatura){
                    foreach ($this->formatoClases($asignatura->clases) as $clase){
                        $clase->asignatura = $asignatura->nombre;
                        $clase->docente_nombre = $asignatura->docente->name;
                        $clase->docente_email = $asignatura->docente->email;
                        array_push($clases,$clase);
                    }
                }
            }
            $user->load('eventos');
            foreach ($this->formatoClases($user->eventos) as $evento){
                $evento->asignatura = $evento->titulo;
                array_push($clases, $evento);
            }
            return Response::responseSuccess(true,CodesResponse::CODE_OK,$clases);
        }catch (ModelNotFoundException $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_NOT_FOUND);
        }catch (\Exception $e){
            return Response::responseError($e->getMessage(), CodesResponse::CODE_INTERNAL_SERVER);
        }
    }

}
