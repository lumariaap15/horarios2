<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function formatoClases($clases)
    {
        foreach ($clases as $clase){
            $clase->hora = $clase->fecha.' '.$clase->hora;
            $clase->hora_fin = $clase->fecha.' '.$clase->hora_fin;
        }
        return $clases;
    }
}
