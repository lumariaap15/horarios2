<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class AsignaturaRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'docente_id'=>'required|exists:users,id',
                'codigo'=>'required|unique:asignaturas,codigo',
                'nombre'=>'required'
            ];
        }
        if ($this->method() == 'PUT'){
            return[
                'docente_id'=>'required|exists:users,id',
                'codigo'=>'required|unique:asignaturas,codigo,'.$this->route('asignatura'),
                'nombre'=>'required'
            ];
        }
    }
}
