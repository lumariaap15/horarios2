<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class AulaRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'nombre'=>'required|unique:aulas,nombre',
            ];
        }
        if ($this->method() == 'PUT'){
            return[
                'nombre'=>'required|unique:aulas,nombre,'.$this->route('aula'),
            ];
        }
    }
}
