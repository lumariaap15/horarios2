<?php

namespace App\Http\Requests;

use App\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class EstudianteRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
        return [
            'identificacion'=>'required|unique:users,identificacion',
            'email'=>'required|unique:users,email',
            'name'=>'required'
        ];
    }
        if ($this->method() == 'PUT'){
            return[
                'identificacion'=>'required|unique:users,identificacion,'.$this->route('estudiante'),
                'email'=>'required|unique:users,email,'.$this->route('estudiante'),
                'name'=>'required'
            ];
        }
    }
}
