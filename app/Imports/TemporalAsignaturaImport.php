<?php

namespace App\Imports;

use App\TemporalAsignatura;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class TemporalAsignaturaImport implements WithHeadingRow, WithValidation, ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TemporalAsignatura([
            'codigo' => $row['codigo']
        ]);
    }

    public function rules(): array
    {
        return [
            'codigo' => 'required|exists:asignaturas,codigo',
        ];
    }
}
