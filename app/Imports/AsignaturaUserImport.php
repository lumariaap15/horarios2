<?php

namespace App\Imports;


use App\Asignatura;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class AsignaturaUserImport implements ToCollection, WithHeadingRow, WithValidation
{
    /**
    * @param Collection $rows
    */
    public function collection(Collection $rows)
    {
        //validar que todos los campos esten
        foreach ($rows as $row){
                //creamos el usuario
                $user = User::query()->where('identificacion', $row['cedula_estudiante'])->first();
                if (!$user) {
                    $user = new User();
                }
                $user->identificacion = $row['cedula_estudiante'];
                $user->email = property_exists($row, 'mail') ? $row['mail'] : $row['nom_tercero'].$row['pri_apellido'].'@ustadistancia.edu.co';
                $user->role = User::ROLE_ESTUDIANTE;
                $user->password = bcrypt($row['cedula_estudiante']);
                $user->name = ucwords($row['nom_tercero'] . ' ' . ($row['seg_nombre'] ? $row['seg_nombre'] : '') . ' ' . $row['pri_apellido'] . ' ' . ($row['seg_apellido'] ? $row['seg_apellido'] : ''));
                $user->save();
                //creamos la asignatura
                $codigo = property_exists($row, 'cod_agrupado') ? $row['cod_agrupado'] : str_replace(' ', '_', $row['nom_materia']) . '00';
                $asignatura = Asignatura::query()->where('codigo', $codigo)->first();
                if (!$asignatura) {
                    $asignatura = new Asignatura();
                }
                $asignatura->codigo = $codigo;
                $asignatura->nombre = $row['nom_materia'];
                $asignatura->save();
                //sincronizamos el usuario
                $asignatura->estudiantes()->syncWithoutDetaching([$user->id]);
        }
    }

    public function rules(): array
    {
        return [
            'cedula_estudiante' => 'required',//'required|unique:users,identificacion',
            'nom_tercero' => 'required',
            'pri_apellido' => 'required',
            //'mail' => 'required|email',//'required|email|unique:users,email',
            'nom_materia' => 'required',
            //'cod_agrupado' => 'required',
        ];
    }
}
