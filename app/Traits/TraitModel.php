<?php

namespace App\Traits;

trait TraitModel{


    public function scopeBuscarCoincidencia($query, $termino){
        foreach ($this->filasBuscar as $column){
            $query->orWhere($column,'like','%'.$termino.'%');
        }
        if($this->relationsBuscar){
            foreach ($this->relationsBuscar as $relation=>$columns){
                foreach ($columns as $column){
                    $query->orWhereHas($relation,function ($query2) use ($termino, $column){
                        $query2->where($column,'like','%'.$termino.'%');
                    });
                }
            }
        }
        return $query;
    }

}
