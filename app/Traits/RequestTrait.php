<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

trait RequestTrait {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Metodo que evita la redireccion en caso de fallo
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     */
    protected function failedValidation(Validator $validator) {

        throw new HttpResponseException(
            response()->json(
                [
                    'error'    => $validator->errors()->all()[0],
                ],
                422
            )
        );

    }
}
