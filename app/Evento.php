<?php

namespace App;

use App\Traits\TraitModel;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    use TraitModel;

    protected $fillable = [
        'hora',
        'hora_fin',
        'fecha',
        'titulo',
        'description',
    ];

    protected $filasBuscar = [
        'hora',
        'hora_fin',
        'fecha',
        'titulo',
    ];

    protected $relationsBuscar = [

    ];

    public function users()
    {
        return $this->belongsToMany(User::class,'evento_user');
    }
}
