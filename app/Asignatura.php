<?php

namespace App;

use App\Traits\TraitModel;
use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    use TraitModel;

    protected $fillable = [
      'nombre',
      'codigo',
      'docente_id'
    ];

    protected $filasBuscar = [
        'nombre',
        'codigo',
    ];

    protected $relationsBuscar = [
        'docente'=>['name']
    ];

    public function clases()
    {
        return $this->hasMany(Clase::class,'asignatura_id','id')->orderBy('fecha');
    }

    public function docente()
    {
        return $this->belongsTo(User::class,'docente_id','id');
    }

    public function estudiantes()
    {
        return $this->belongsToMany(User::class,'asignatura_user');
    }
}
