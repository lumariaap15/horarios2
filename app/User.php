<?php

namespace App;

use App\Traits\TraitModel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use TraitModel;

    const ROLE_ADMIN = "Administrador";
    const ROLE_DOCENTE = "Docente";
    const ROLE_ESTUDIANTE = "Estudiante";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'identificacion',
    ];

    protected $filasBuscar = [
        'name',
        'email',
        'identificacion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function asignaturasEstudiante()
    {
        return $this->belongsToMany(Asignatura::class,'asignatura_user');
    }

    public function eventos()
    {
        return $this->belongsToMany(Evento::class,'evento_user');
    }

    public function asignaturasDocente()
    {
        return $this->hasMany(Asignatura::class,'docente_id','id');
    }

    public function getJWTIdentifier() {
        return  $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function hasRole($role){
        return $this->role == $role;
    }

    public function isAdmin(){
        return $this->role === self::ROLE_ADMIN;
    }
}
