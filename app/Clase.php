<?php

namespace App;

use App\Traits\TraitModel;
use Illuminate\Database\Eloquent\Model;

class Clase extends Model
{
    use TraitModel;

    protected $fillable = [
        'hora',
        'hora_fin',
        'fecha',
        'asignatura_id',
        'aula_id',
        'description',
    ];

    protected $filasBuscar = [
        'hora',
        'hora_fin',
        'fecha',
    ];

    protected $relationsBuscar = [
        'aula'=>['nombre'],
        'asignatura'=>['nombre','codigo'],
    ];

    public function aula()
    {
        return $this->belongsTo(Aula::class,'aula_id','id');
    }

    public function asignatura()
    {
        return $this->belongsTo(Asignatura::class,'asignatura_id','id');
    }
}
