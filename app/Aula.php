<?php

namespace App;

use App\Traits\TraitModel;
use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    use TraitModel;

    protected $fillable = [
        'nombre',
        'cantidad_docentes',
    ];

    protected $filasBuscar = [
        'nombre',
        'cantidad_docentes',
    ];

    public function horarios()
    {
        return $this->hasMany(HorariosAula::class,'aula_id','id');
    }

    public function clases()
    {
        return $this->hasMany(Clase::class,'aula_id','id');
    }
}
