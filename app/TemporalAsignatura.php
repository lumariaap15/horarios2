<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemporalAsignatura extends Model
{
    protected $fillable = ['codigo'];
}
