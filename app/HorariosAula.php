<?php

namespace App;

use App\Traits\TraitModel;
use Illuminate\Database\Eloquent\Model;

class HorariosAula extends Model
{
    use TraitModel;

    protected $fillable = [
        'dia_semana',
        'hora_inicio',
        'hora_fin',
        'aula_id',
    ];

    protected $filasBuscar = [
        'dia_semana',
        'hora_inicio',
        'hora_fin',
    ];

    protected $relationsBuscar = [
        'aula'=>['nombre','cantidad_docentes'],
    ];

    public function aula()
    {
        return $this->belongsTo(Aula::class,'aula_id','id');
    }
}
