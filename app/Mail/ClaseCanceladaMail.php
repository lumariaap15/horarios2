<?php

namespace App\Mail;

use App\Parameter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClaseCanceladaMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $logoInstitucion = Parameter::query()->first();
        $this->data->logo_institucion = $logoInstitucion->logo_institucion;
        return $this->from('skedul.info@gmail.com')
            ->with('data',$this->data)
            ->subject('Tu clase de '.$this->data->asignatura->nombre.' fue cancelada')
            ->view('mail.clase_cancelada');
    }
}
