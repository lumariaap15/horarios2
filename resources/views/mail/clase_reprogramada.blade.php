<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clase reprogramada</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">
    <style>
        @font-face{
            font-family:'Poppins';
            font-style:normal;
            font-weight:400;
            src:url('https://fonts.googleapis.com/css2?family=Poppins:wght@400&display=swap');
        }

        @font-face{
            font-family:'Poppins';
            font-style:normal;
            font-weight:700;
            src:url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap');
        }


        #main-table{
            border-collapse: collapse;
            margin-top: 60px;
            margin-bottom: 60px;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">
<table style="background: #1565c0" border="0" cellpadding="0" cellspacing="0" width="100%">
    <td>
        <table id="main-table" align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
            <tr>

                <td align="center" style="padding: 20px 0;">
                    <img src="{{asset('img/logo-skedul.png')}}" alt="logo skedul"
                         width="300"
                         style="display: block;"/>
                </td>

            </tr>
            <tr>

                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">

                    <table  border="1" cellpadding="0" cellspacing="0" width="100%" style="border-color: #1565c0; border-collapse: collapse;">
                        <tr>

                            <td align="center" style="padding: 20px 0;">
                                <img src="{{config('app.url')}}/storage/{{$data->logo_institucion}}" alt="logo institucion"
                                     width="220"
                                     style="display: block;"/>
                            </td>

                        </tr>
                        <tr>

                            <td align="center"
                                style=" padding: 20px 10px;color: #343a40; font-size: 30px; font-family: 'Poppins','sans-serif';">

                                <b>Tu clase de {{$data->clase->asignatura->nombre}} fue
                                    <span style="color: #ff9800 "> reprogramada</span>
                                </b>

                            </td>

                        </tr>
                        <tr>
                            <td align="center" style=" padding: 20px 10px;font-size: 16px; font-family: 'Poppins','sans-serif';">
                                <p>Fecha: <b>{{$data->clase->fecha}}
                                        @if ($data->clase->fecha !== $data->claseNueva->fecha)
                                        &rarr;
                                            <span style="color: #ff9800 ">{{$data->claseNueva->fecha}}</span>
                                        @endif
                                    </b>
                                </p>
                                <p>Hora: <b>{{\Carbon\Carbon::createFromTimeString($data->clase->hora)->format('h:i a')}}
                                        @if ($data->clase->hora !== $data->claseNueva->hora)
                                         &rarr;
                                            <span style="color: #ff9800 ">
                                            {{\Carbon\Carbon::createFromTimeString($data->claseNueva->hora)->format('h:i a')}}
                                            </span>
                                        @endif
                                    </b>
                                </p>
                                <p>Salón: <b>{{$data->clase->aula->nombre}}
                                        @if($data->clase->aula_id !== $data->claseNueva->aula_id)
                                        &rarr;
                                            <span style="color: #ff9800 ">{{$data->claseNueva->aula->nombre}}</span>
                                        @endif
                                    </b>
                                </p>
                            </td>
                        </tr>

                        <tr>

                            <td align="center" style="padding: 20px 0 15px 0; color: #343a40; font-size: 14px; background: #e6e6e6;font-family: 'Poppins','sans-serif'; line-height: 20px;">
                                Desarrollado con ❤️ por <a target="_blank" href="https://www.linkedin.com/in/luisa-mar%C3%ADa-alzate-parrado-89a88a17a/">Luisa María Alzate</a> - {{ \Carbon\Carbon::now()->format('Y') }} © Derechos Reservados.
                            </td>

                        </tr>
                    </table>

                </td>

            </tr>
        </table>
    </td>
</table>
</body>
</html>
