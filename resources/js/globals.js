export const ALERTS = {
    error:{
        form: "Tienes errores en el formulario",
        internal: "Lo sentimos, ocurrió un error inesperado. Por favor inténtelo más tarde.",
    },
    success:{
        update:"¡Bien! La actualización se realizó correctamente.",
        create:"¡Bien! El registro fue creado correctamente."
    },
    warning:{
        delete:"¿Deseas eliminar este registro?"
    }
};

export const ROLES = {
    admin:"Administrador",
    docente:"Docente",
    estudiante:"Estudiante",
};
