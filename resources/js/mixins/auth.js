export const auth = {
    computed:{
      authUser(){
          return JSON.parse(localStorage.getItem('user'))
      }
    },
    methods: {
        hasRoles(roles){
            return roles.includes(this.authUser.role);
        }
    }
};
