export const utils = {
    data(){
      return {
          colors: ['blue','pink', 'indigo', 'deep-purple','orange','green', 'amber', 'teal','cyan','purple','lime','brown','deep-orange','blue-grey']
      }
    },
    methods: {
        getColorIdx(idx){
            let numVeces = Math.trunc(idx/this.colors.length);
            let realIdx = idx - (this.colors.length * numVeces);
            return this.colors[realIdx];
        },
        randomColor(){
            let colors =  this.colors;
            let a = 0;
            let b = colors.length -1;
            let idx = Math.floor((b - a + 1) * Math.random()) + a;
            return colors[idx];
        }
    }
};
