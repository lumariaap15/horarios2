import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        duracion_clase:0,
        logo_institucion:"",
    },
    mutations:{
        setDuracionClase(state, duracion){
            state.duracion_clase = duracion
        },
        setLogoInstitucion(state, logo){
            state.logo_institucion = logo
        },
    },
    getters:{

    }
});
