import VueRouter from 'vue-router'
//admin
import AdminDocentes from './pages/admin/docentes/docentes';
import AdminAsignaturas from './pages/admin/asignaturas/asignaturas';
import AdminAsignaturasDetail from './pages/admin/asignaturas/asignaturaDetailAdmin';
import AdminClases from './pages/admin/clases/clases';
import AdminConfig from './pages/admin/configuracion/configuracion';
import AdminEstudiantes from './pages/admin/estudiantes/estudiantes';
import AdminEventos from './pages/admin/eventos/eventos';
import AdminSalones from './pages/admin/salones/salones';
import DocenteDetail from "./pages/admin/docentes/show/DocenteDetail";
import EstudianteDetail from "./pages/admin/estudiantes/show/EstudianteDetail";

import layout from "./pages/layout";
import login from "./pages/login";
import {ROLES} from "./globals";
//dashboard
import AsignaturaDetail from "./pages/dashboard/asignaturas/AsignaturaDetail";
import Asignaturas from './pages/dashboard/asignaturas/asignaturas';
import Clases from './pages/dashboard/clases/clases';
import Perfil from './pages/dashboard/perfil/perfil';
import claseFormCreate from "./pages/admin/clases/claseFormCreate";


const router = new VueRouter({
    mode: 'history',
    routes: [
        {path:'/', redirect: '/admin/docentes'},
        {
            path:'/login',
            component: login,
            name: 'login',
            meta:{roles:[]}
        },
        {
            path:'/dashboard',
            component: layout,
            name: 'dashboard',
            children:[
                {
                    path:'asignaturas',
                    component: Asignaturas,
                    name:'dashboard-asignaturas',
                    meta:{roles:[ROLES.docente, ROLES.estudiante]}
                },
                {
                    path:'asignaturas/:id',
                    component: AsignaturaDetail,
                    name:'dashboard-asignaturas-show',
                    meta:{roles:[ROLES.docente, ROLES.estudiante]}
                },
                {
                    path:'clases',
                    component: Clases,
                    name:'dashboard-clases',
                    meta:{roles:[ROLES.docente, ROLES.estudiante]}
                },
                {
                    path:'perfil',
                    component: Perfil,
                    name:'dashboard-perfil',
                    meta:{roles:[ROLES.docente, ROLES.estudiante]}
                },
            ]
        },
        {
            path: '/admin',
            component: layout,
            children:[
                {
                    path:'docentes',
                    component: AdminDocentes,
                    name:'admin-docentes',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'docentes/:id',
                    component: DocenteDetail,
                    name:'admin-docentes-show',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'estudiantes',
                    component: AdminEstudiantes,
                    name:'admin-estudiantes',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'estudiantes/:id',
                    component: EstudianteDetail,
                    name:'admin-estudiantes-show',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'asignaturas',
                    component: AdminAsignaturas,
                    name:'admin-asignaturas',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'asignaturas/:id',
                    component: AdminAsignaturasDetail,
                    name:'admin-asignaturas-show',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'salones',
                    component: AdminSalones,
                    name:'admin-salones',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'clases',
                    component: AdminClases,
                    name:'admin-clases',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'clases/programar',
                    component: claseFormCreate,
                    name:'admin-clases-programar',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'configuracion',
                    component: AdminConfig,
                    name:'admin-configuracion',
                    meta:{roles:[ROLES.admin]},
                },
                {
                    path:'eventos',
                    component: AdminEventos,
                    name:'admin-eventos',
                    meta:{roles:[ROLES.admin]},
                },
            ]
        },
    ]
});


router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token');
    const user = JSON.parse(localStorage.getItem('user'));
    if (token === null && to.path !== '/login') {
        //SI NO ESTÁ AUTENTICADO SE REDIRECCIONA AL LOGIN
        next({name: 'login'})
    } else if (token && to.path === '/login') {
        //SI ESTÁ AUTENTICADO Y TRATA DE IR AL LOGIN SE REDIRECCIONA AL HOME
        redirectHome(user, next)
    } else if (token && to.path !== '/login') {
        //SI ESTÁ AUTENTICADO VERIFICAR PERMISOS
        if(to.meta.roles.length > 0){
            if(to.meta.roles.includes(user.role)) {
                // CON PERMISOS
                next();
            }else{
                // SIN PERMISOS
                redirectHome(user, next)
            }
        }else{
            // CON PERMISOS
            next();
        }
    } else {
        next();
    }
});

const redirectHome = (user, next)=>{
    if(user.role === ROLES.admin){
        next({name:"admin-docentes"})
    }else{
        next({name:"dashboard-clases"})
    }
};



export default router;
