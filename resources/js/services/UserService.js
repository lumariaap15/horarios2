import axios from './GlobalService';

const UserService = {
    all() {
        return axios.get('users');
    },
    restorePassword(id) {
        return axios.put('user/restore-password/'+id);
    },
    changePassword(id, data) {
        return axios.post('user/change-password/'+id, data);
    },
    misClases(id) {
        return axios.get('user/clases/'+id);
    },
    misAsignaturas(id) {
        return axios.get('user/asignaturas/'+id);
    },
};

export default UserService;
