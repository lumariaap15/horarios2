import axios from 'axios';
axios.defaults.baseURL = '/api';

import router from "../router";

//interceptamos para que siempre envie el token al post
axios.interceptors.request.use(function (config) {
    if (config.url !== 'login') {
        const auth_token = localStorage.getItem('token');
        if (auth_token) {
            config.headers.Authorization = `Bearer ${auth_token}`;
        }
    }
    return config;
}, function (err) {
    return Promise.reject(err);
});

//Redireccionamos al login si el token expiró
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 401) {
        if (error.response.config.url !== 'login') {
            localStorage.clear();
            window.location.reload();
        }
    }else if(error.response.status === 403){
        router.push({path:'/'})
    }
    return Promise.reject(error);
});

//Redireccionamos si alguna petición nos envía un 404
/*
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 404) {
        window.location.href = '/404'
    }
    return Promise.reject(error);
});
 */

export default axios
