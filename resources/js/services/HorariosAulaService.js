import axios from './GlobalService';

const HorariosAulaService = {
    all(query) {
        return axios.get('horarios_aulas'+query);
    },
    store(data) {
        return axios.post('horarios_aulas', data);
    },
    find(id) {
        return axios.get(`horarios_aulas/${id}`);
    },
    update(id, data) {
        return axios.put(`horarios_aulas/${id}`, data);
    },
    delete(id) {
        return axios.delete(`horarios_aulas/${id}`);
    },
};

export default HorariosAulaService;
