import axios from './GlobalService';

const EstudiantesService = {
    all(query) {
        return axios.get('estudiantes'+query);
    },
    store(data) {
        return axios.post('estudiantes', data);
    },
    find(id) {
        return axios.get(`estudiantes/${id}`);
    },
    update(id, data) {
        return axios.put(`estudiantes/${id}`, data);
    },
    delete(id) {
        return axios.delete(`estudiantes/${id}`);
    },
};

export default EstudiantesService;
