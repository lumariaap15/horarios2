import axios from './GlobalService';

const AsignaturaService = {
    all(query) {
        return axios.get('asignaturas'+query);
    },
    store(data) {
        return axios.post('asignaturas', data);
    },
    find(id) {
        return axios.get(`asignaturas/${id}`);
    },
    update(id, data) {
        return axios.put(`asignaturas/${id}`, data);
    },
    delete(id) {
        return axios.delete(`asignaturas/${id}`);
    },
};

export default AsignaturaService;
