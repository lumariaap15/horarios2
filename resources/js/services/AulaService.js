import axios from './GlobalService';

const AulaService = {
    all() {
        return axios.get('aulas');
    },
    store(data) {
        return axios.post('aulas', data);
    },
    find(id) {
        return axios.get(`aulas/${id}`);
    },
    update(id, data) {
        return axios.put(`aulas/${id}`, data);
    },
    delete(id) {
        return axios.delete(`aulas/${id}`);
    },
};

export default AulaService;
