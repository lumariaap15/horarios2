import axios from './GlobalService';

const AuthService = {
    login(data) {
        return axios.post('login', data);
    },
    logout() {
        return axios.post('logout');
    },
    refresh() {
        return axios.post('refresh');
    },
};

export default AuthService;
