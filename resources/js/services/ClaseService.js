import axios from './GlobalService';

const ClaseService = {
    all(query) {
        return axios.get('clases'+query);
    },
    programar(data, sendFile = false) {
        let config = {
            headers: {
                'Content-Type': (sendFile ? 'multipart/form-data' : 'application/json')
            }
        };
        return axios.post('clases/programar', data, config);
    },
    sugerirHorasAsignatura(data, claseId = '') {
        claseId = claseId !== '' ? '/'+claseId : '';
        return axios.post('clases/sugerir-horas-asignatura'+claseId, data);
    },
    sugerirSalones(data, claseId = '') {
        claseId = claseId !== '' ? '/'+claseId : '';
        return axios.post('clases/sugerir-salones'+claseId, data);
    },
    programarClaseIndividual(data, claseId = '') {
        claseId = claseId !== '' ? '/'+claseId : '';
        return axios.post('clases/programar-individual'+claseId, data);
    },
    cancelarClase(id) {
        return axios.delete('clases/cancelar/'+id);
    },
    updateDescription(claseId, description) {
        return axios.post('clases/update-description/'+claseId, {description: description});
    },
};

export default ClaseService;
