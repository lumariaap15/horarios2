import axios from './GlobalService';

const GeneralService = {
    loadData(data) {
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        };
        return axios.post('general/load-data', data, config);
    },
    getParameters(){
        return axios.get('general/parameters')
    },
    updateParameters(data){
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        };
        return axios.post('general/parameters-update', data, config)
    }
};

export default GeneralService;
