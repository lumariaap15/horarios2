import axios from './GlobalService';

const EventosService = {
    all(query) {
        return axios.get('eventos'+query);
    },
    store(data) {
        return axios.post('eventos', data);
    },
    find(id) {
        return axios.get(`eventos/${id}`);
    },
    update(id, data) {
        return axios.put(`eventos/${id}`, data);
    },
    delete(id) {
        return axios.delete(`eventos/${id}`);
    },
};

export default EventosService;
