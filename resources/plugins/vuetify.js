// You still need to register Vuetify itself
// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify);

const opts = {
    theme:{
        themes:{
            light:{
                primary: colors.blue.darken3,
                secondary: colors.blue.base,
                accent: colors.orange.base,
                error: colors.red.base,
                warning: colors.amber.base,
                info: colors.cyan.base,
                success: colors.green.base
            }
        }
    }
};

export default new Vuetify(opts)
