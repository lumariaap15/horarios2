<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosAulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios_aulas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('aula_id');
            $table->enum('dia_semana',['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo']);
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->foreign('aula_id')->references('id')->on('aulas')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios_aulas');
    }
}
