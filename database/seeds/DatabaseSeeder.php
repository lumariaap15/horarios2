<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 1)->create(['role'=>'Administrador','email'=>'admin@ustadistancia.edu.co']);
        //factory(\App\User::class, 11)->create(['role'=>'Docente']);
        //factory(\App\User::class, 11)->create(['role'=>'Estudiante']);
        $this->call(DatosSeeder::class);
    }
}
