<?php

use App\Asignatura;
use App\Aula;
use App\Clase;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatosPruebaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ASIGNATURAS
        $asignaturas = Asignatura::query()->get();
        //DOCENTES
        factory(\App\User::class, 50)->create(['role'=>'Docente']);
        $docenteId = 348;
        $docenteIdx = 0;
        while ($docenteIdx < 50){
            $asignaturasCount = 0;
            if($docenteIdx === 49){
                while ($asignaturasCount <= 11){
                    $selector = ($docenteIdx * 10) + $asignaturasCount;
                    $asignaturas[$selector]->docente_id = $docenteId;
                    $asignaturas[$selector]->save();
                    $asignaturasCount++;
                }
            }else{
                while ($asignaturasCount <= 10){
                    $selector = ($docenteIdx * 10) + $asignaturasCount;
                    $asignaturas[$selector]->docente_id = $docenteId;
                    $asignaturas[$selector]->save();
                    $asignaturasCount++;
                }
            }
            $docenteIdx++;
            $docenteId++;
        }
    }
}
